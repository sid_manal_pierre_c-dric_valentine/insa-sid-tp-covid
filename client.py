#!/usr/bin/env python3
from message import Message
from message_catalog import MessageCatalog
import requests

class Client:
	#constructor, takes a message catalog
	def __init__(self, catalog_path, debug = False, defaultProtocol="http://"):
		self.catalog = MessageCatalog(catalog_path) #création du catalogue de messages du client
		if debug:
			print(self.catalog)
		self.r = None #initialise la requête du client
		self.debug = debug
		self.protocol = defaultProtocol #définit le début de chaque URL pour une question de confort. Ne pas avoir à taper http:// à chaque fois. 

	@classmethod
	def withProtocol(cls, host):
		res = host.find("://") > 1
		return res

	def completeUrl(self, host, route = ""):
		if not Client.withProtocol(host):
			host = self.protocol + host
		if route != "":
			route = "/"+route
		return host+route

	#send an I said message to a host
	def say_something(self, host): #Ajoute un message du client dans son catalogue.
		m = Message() #crée un message aleatoire
		self.catalog.add_message(m) #ajoute le message dans le catalogue du client. Regroupe tous les messages envoyés par la personne.
		route = self.completeUrl(host, m.content) #ajoute http:// si pas présent et ajoute le message après le /
		self.r = requests.post(route) #envoie le message de type I said au host
		if self.debug:
			print("POST  "+route + "→" + str(self.r.status_code))
			print(self.r.text) #affiche le texte reçu par le host
		return self.r.status_code == 201 #renvoie True si le status_code est égal à 201. Ce statut signifie que la requête a fonctionné.

	#add to catalog all the covid from host server
	def get_covid(self, host): #Recupere le catalogue they said pour l'importer dans le catalogue du client.
		route = self.completeUrl(host,'/they-said') #définit la route http://they-said
		self.r = requests.get(route) #récupère le catalogue they said
		res = self.r.status_code == 200 #booleen pour savoir si la requête a fonctionné
		if res:
			res = self.catalog.c_import(self.r.json())#importe tous les messages "they said" dans le catalogue du client sous format json.
		if self.debug:
			print("GET  "+ route + "→" + str(self.r.status_code))
			if res != False:
				print(str(self.r.json()))
		return res

	#send to server list of I said messages
	def send_history(self, host): #Un client ayant le covid envoie tous ses messages à l'hôpital qui les mets dans un catalogue they said.
		route = self.completeUrl(host,'/they-said') #le destinataire est l'hopîtal
		self.catalog.purge(Message.MSG_ISAID) #supprime les messages de type Isaid du catalogue du client datant de plus de 14jours
		data = self.catalog.c_export_type(Message.MSG_ISAID) #exporte tous les messages de type Isaid du catalogue.
		self.r = requests.post(route, json=data) #envoie ce qui a été exporté du catalogue Isaid dans le catalogue TheySaid.
		if self.debug:
			print("POST  "+ route + "→" + str(self.r.status_code)) #vérifie que le message a bien été reçu
			print(str(data)) #vérifie que les bonnes datas ont été exportées
			print(str(self.r.text)) #affiche le texte reçu par l'hopital
		return self.r.status_code == 201 #renvoie True si le status_code est égal à 201. Ce statut signifie que la requête a fonctionné.


if __name__ == "__main__":
	c = Client("client.json", True)
	c.say_something("localhost:5000")
	c.get_covid("localhost:5000")
	c.send_history("localhost:5000")
