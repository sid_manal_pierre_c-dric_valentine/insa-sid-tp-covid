#!/usr/bin/env python3
from flask import Flask #server
from flask import request #to handle the different http requests
from flask import Response #to reply (we could use jsonify as well but we handled it)
from flask import render_template #to use external files for responses (out of the scope of the project, added to provide a test UI)
import json
#My libraries
from message import Message
from client import Client #embarks message_catalog

app = Flask(__name__)

#“Routes” will handle all requests to a specific resource indicated in the
#@app.route() decorator

#These route are for the "person" use case
	#case 1 refuse GET
@app.route('/', methods=['GET'])
def index(): #utilisee pour la partie UI
	response = render_template("menu.html",root=request.url_root, h="")
	return response

	#case 2 hear message
@app.route('/<msg>', methods=['POST']) #la fonction add_heard a comme ressource un message avec uniquement la methode post autorisee
def add_heard(msg): #fonction pour afficher la reponse du serveur pour les messages i heard
	if client.catalog.add_message(Message(msg, Message.MSG_IHEARD)): #si un message de type i heard a ete ajoute dans le catalogue client
		response = Response(f"Message “{msg}” received.", mimetype='text/plain', status=201) #Reponse du serveur qui affiche le message entendu
	else :
		response = Response(status=400) #code d'erreur 400 si le message n'est pas de type i heard
	return response
#End of person use case

#Hospital use case
@app.route('/they-said', methods=['GET','POST']) #fonction hospital a comme ressource le catalogue they said avec les methodes get et post autorisees
def hospital(): #fonction pour recevoir une liste de s ou envoyer une liste de messages
	if request.method == 'GET': #si on veut envoyer la liste de messages they said
		#Wants the list of covid messages (they said)
		client.catalog.purge(Message.MSG_THEY) #supprime les messages de type theysaid du catalogue du client datant de plus de 14jours
		response = Response(client.catalog.c_export_type(Message.MSG_THEY), mimetype='application/json', status=200) #envoie au client les messages they said datant de moins de 14 jours
	elif request.method == 'POST': #l'hopital recoit une liste de messages i said
		if request.is_json: #verifie que les donnees sont bien de type json
			req = json.loads(request.get_json())#charge les donnes envoyees par le client
			response = client.catalog.c_import_as_type(req, Message.MSG_THEY)#importe les messages they said presents dans les donnees chargees juste avant dans le catalogue client
			response = Response(f"{response} new “they said” messages.", mimetype='text/plain', status=201)#affiche que le serveur a bien reçu de nouveaux messages they said
		else:
			response = Response(f"JSON expected", mimetype='text/plain', status=400)#message d'erreur pour dire que les messages doivent etre sous format json
	else:
		reponse = Response(f"forbidden method “{request.method}”.",status=403)#message d'erreur si une methode non autorisee est utilisee
	return response
#End hospital use case

#Begin UI
#These routes are out of the scope of the project, they are here
#to provide a test interface
@app.route('/check/<host>', methods=['GET'])
def check(host):
	h = host.strip()
	n = client.get_covid(h)
	r = dict()
	r["text"] = f"{n} they said messages imported from {h} ({client.r.status_code})"
	if client.catalog.quarantine(4):
		r["summary"] = "Stay home, you are sick."
	else:
		r["summary"] = "Everything is fine, but stay home anyway."
	return render_template("menu.html",responses=[r],root=request.url_root, h=h)

@app.route('/declare/<host>', methods=['GET'])
def declare(host):
	h = host.strip()
	client.send_history(h)
	r=[{"summary":f"Declare covid to {h} ({client.r.status_code})",
	"text":client.r.text}]
	return render_template("menu.html",responses=r,root=request.url_root, h=h)

@app.route('/say/<hosts>', methods=['GET'])
def tell(hosts):
	hosts = hosts.split(",")
	r=[]
	for host in hosts:
		h = host.strip()
		client.say_something(h)
		r.append({"summary":f"ISAID to {h} ({client.r.status_code})",
		"text":client.r.text})
	return render_template("menu.html", responses=r, root=request.url_root, h=h)
#end UI

#will only execute if this file is run
if __name__ == "__main__":
	debugging = True
	client = Client("client.json", debugging)
	app.run(host="0.0.0.0", debug=debugging)
